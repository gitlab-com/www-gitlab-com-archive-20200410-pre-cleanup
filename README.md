# www-gitlab-com-archive-20200410-pre-cleanup

This is an archive of the gitlab-com/www-gitlab-com repo and all branches, as of 2020-04-10, prior to deleting stale branches and other cleanup to reduce the main repo size.

If you want to restore a branch which existed in the https://gitlab.com/gitlab-com/www-gitlab-com/ repo prior to that date, but is no longer there, you can do the following:

* Clone this repo or add it as a remote to your main clone of `www-gitlab-com`
* Re-push the branch back to the `www-gitlab-com` repo.

See https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/5525 for details.